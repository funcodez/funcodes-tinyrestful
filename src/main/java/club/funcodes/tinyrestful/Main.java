// /////////////////////////////////////////////////////////////////////////////
// FUNCODES.CLUB
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package club.funcodes.tinyrestful;

import static org.refcodes.cli.CliSugar.*;
import static org.refcodes.rest.HttpRestServerSugar.*;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

import org.refcodes.archetype.CliHelper;
import org.refcodes.cli.CliSugar;
import org.refcodes.cli.Example;
import org.refcodes.cli.Flag;
import org.refcodes.cli.Option;
import org.refcodes.cli.Term;
import org.refcodes.data.AsciiColorPalette;
import org.refcodes.exception.Trap;
import org.refcodes.logger.RuntimeLogger;
import org.refcodes.logger.RuntimeLoggerFactorySingleton;
import org.refcodes.net.PortManagerSingleton;
import org.refcodes.properties.ext.application.ApplicationProperties;
import org.refcodes.rest.HttpRestServer;
import org.refcodes.rest.HttpRestServerSugar;
import org.refcodes.rest.RestfulHttpServer;
import org.refcodes.runtime.Correlation;
import org.refcodes.runtime.Host;
import org.refcodes.textual.Font;
import org.refcodes.textual.FontFamily;
import org.refcodes.textual.FontStyle;
import org.refcodes.web.BasicAuthCredentials;
import org.refcodes.web.BasicAuthResponse;
import org.refcodes.web.HttpMethod;
import org.refcodes.web.InternalServerErrorException;
import org.refcodes.web.MediaType;

/**
 * Demo application on how to use command line argument parsing with ease using
 * {@link CliSugar} and on how to set up a simple RESTful service with ease
 * using {@link HttpRestServerSugar}.
 */
public class Main {

	private static final RuntimeLogger LOGGER = RuntimeLoggerFactorySingleton.createRuntimeLogger();;

	// /////////////////////////////////////////////////////////////////////////
	// STATICS:
	// /////////////////////////////////////////////////////////////////////////

	private static final String NAME = "tinyrestful";
	private static final String TITLE = "TINY!RESTFUL?";
	private static final String DEFAULT_CONFIG = NAME + ".ini";
	private static final String LICENSE_NOTE = "Licensed under GNU General Public License, v3.0 and Apache License, v2.0";
	private static final String COPYRIGHT = "Copyright (c) by CLUB.FUNCODES (see [https://www.funcodes.club])";
	private static final char[] BANNER_PALETTE = AsciiColorPalette.MAX_LEVEL_GRAY.getPalette();
	private static final Font BANNER_FONT = new Font( FontFamily.DIALOG, FontStyle.BOLD );
	private static final String DESCRIPTION = "Tiny evil RESTful server. Get inspired by [https://bitbucket.org/funcodez].";

	private static final int SHUTDOWN_LATENCY_MILLIS = 300;

	// /////////////////////////////////////////////////////////////////////////
	// METHODS:
	// /////////////////////////////////////////////////////////////////////////

	public static void main( String args[] ) {

		// ---------------------------------------------------------------------
		// CLI:
		// ---------------------------------------------------------------------

		final Option<Integer> thePortOption = intOption( 'p', "port", "port", "Sets the port for the server" );
		final Option<Integer> theMaxConns = intOption( 'c', "connections", "connections", "Sets the number of max. connections" );
		final Option<String> theUsername = stringOption( 'u', "user", "username", "The username for HTTP Basic-Authentication" );
		final Option<String> theSecret = stringOption( 's', "secret", "secret", "The password for HTTP Basic-Authentication" );
		final Option<Integer> theMgmPortOption = intOption( 'm', "management-port", "management-port", "The management-port on which to listen for shutdown..." );
		final Flag theSysInfoFlag = sysInfoFlag( false );
		final Flag theHelpFlag = helpFlag();

		// @formatter:off
		final Term theArgsSyntax = any ( xor( 
			optional( thePortOption, theMaxConns, and( theUsername, theSecret ), theMgmPortOption ),
			any( xor( theHelpFlag, theSysInfoFlag ) ) )
		);
		final Example[] theExamples = examples(
			example( "To use a specific port", thePortOption ),
			example( "To secure with a user's credentials", theUsername, theSecret ),
			example( "To show the help text", theHelpFlag ),
			example( "To print the system info", theSysInfoFlag )
		);
		final CliHelper theCliHelper = CliHelper.builder().
			withArgs( args ).
			// withArgs( args, ArgsFilter.D_XX ).
			withArgsSyntax( theArgsSyntax ).
			withExamples( theExamples ).
			withFilePath( DEFAULT_CONFIG ). // Must be the name of the default (template) configuration file below "/src/main/resources"
			withResourceClass( Main.class ).
			withName( NAME ).
			withTitle( TITLE ).
			withDescription( DESCRIPTION ).
			withLicense( LICENSE_NOTE ).
			withCopyright( COPYRIGHT ).
			withBannerFont( BANNER_FONT ).
			withBannerFontPalette( BANNER_PALETTE ).
			withLogger( LOGGER ).build();
		// @formatter:on

		final ApplicationProperties theArgsProperties = theCliHelper.getApplicationProperties();

		// ---------------------------------------------------------------------
		// MAIN:
		// ---------------------------------------------------------------------

		try {
			final int theMaxConnections = theMaxConns.getValue() != null ? theMaxConns.getValue() : -1;
			final BasicAuthCredentials theCredentials;
			if ( theUsername.getValue() != null || theSecret.getValue() != null ) {
				theCredentials = new BasicAuthCredentials( theUsername.getValue(), theSecret.getValue() );
			}
			else {
				theCredentials = null;
			}

			onGet( "/backdoor/${command}", ( aRequest, aResponse ) -> {
				try {
					// ---------------------------------------------------------
					// Think about unified way to pass (if set) any header's 
					// correlation IDs to Correlation enumeration:
					// ---------------------------------------------------------
					Correlation.REQUEST.pullId();
					// ---------------------------------------------------------
					String theCommand = aRequest.getWildcardReplacement( "command" );
					String theOptions = "";
					String eValue;
					for ( String eKey : aRequest.getUrl().getQueryFields().keySet() ) {
						eValue = aRequest.getUrl().getQueryFields().getFirst( eKey );
						theOptions += " " + eKey + ( eValue.length() > 0 ? eValue : "" );
					}
					theCommand += theOptions;
					LOGGER.info( "Executing command <" + theCommand + "> ..." );
					aResponse.setResponse( Host.exec( theCommand ) );
				}
				catch ( Exception e ) {
					throw new InternalServerErrorException( Trap.asMessage( e ) );
				}
			} ).open();

			if ( theCredentials != null ) {
				onBasicAuthRequest( ( InetSocketAddress aLocalAddress, InetSocketAddress aRemoteAddress, HttpMethod aHttpMethod, String aLocator, BasicAuthCredentials aCredentials, String aRealm ) -> {
					// ---------------------------------------------------------
					// Think about unified way to pass (if set) any header's 
					// correlation IDs to Correlation enumeration:
					// ---------------------------------------------------------
					Correlation.REQUEST.pullId();
					// ---------------------------------------------------------
					String theAuth = aCredentials != null ? aCredentials.toString() : "(unknown)";
					LOGGER.info( "Requesting Basic-Authentication from <" + aRemoteAddress.toString() + "> for " + theAuth + " on " + aLocalAddress.toString() + " with realm <" + aRealm + "> ." );
					final BasicAuthCredentials theRequestCredentials = aCredentials;
					if ( !theRequestCredentials.equals( theCredentials ) ) {
						return BasicAuthResponse.BASIC_AUTH_FAILURE;
					}
					return BasicAuthResponse.BASIC_AUTH_SUCCESS;
				} );
			}

			final int thePort = thePortOption.getValue() != null ? thePortOption.getValue() : PortManagerSingleton.getInstance().bindAnyPort();
			LOGGER.info( "Starting up on port <" + thePort + "> ..." );
			open( thePort, theMaxConnections );

			final int theMgmPort = theMgmPortOption.getValue() != null ? theMgmPortOption.getValue() : PortManagerSingleton.getInstance().bindAnyPort();
			openManagementServer( theMgmPort );
		}
		catch ( IOException e ) {
			theArgsProperties.printSynopsis();
			theArgsProperties.errorLn( e.getMessage() );
			System.exit( e.hashCode() % 0xFF );
		}
	}

	// /////////////////////////////////////////////////////////////////////////
	// DAEMONS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Enable shutdown-hook at "/shutdown"
	 * 
	 * @param aMgmPort The port where to register the shutdown-hook.
	 */
	private static void openManagementServer( int aMgmPort ) throws IOException {
		LOGGER.info( "Starting management server on port <" + aMgmPort + "> ..." );
		final RestfulHttpServer theRestServer = new HttpRestServer();

		theRestServer.onGet( "/shutdown", ( aRequest, aResponse ) -> { LOGGER.info( "Shutting down ..." ); aResponse.getHeaderFields().putContentType( MediaType.APPLICATION_JSON ); aResponse.setResponse( "Shutting down ... bye!" ); theRestServer.closeIn( SHUTDOWN_LATENCY_MILLIS ); Executors.newSingleThreadScheduledExecutor().schedule( () -> { System.exit( 0 ); }, SHUTDOWN_LATENCY_MILLIS, TimeUnit.MICROSECONDS ); } ).open();
		theRestServer.open( aMgmPort );
	}
}
